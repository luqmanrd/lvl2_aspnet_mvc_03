﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LVL2_ASPNet_MVC_03.Models;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        DB_CUSTOMER_LUQMANEntities db = new DB_CUSTOMER_LUQMANEntities();
        
        // GET: Customer
        public ActionResult Index()
        {
            return View(db.TBL_CUSTOMER.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(db.TBL_CUSTOMER.Where(x => x.ID == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(TBL_CUSTOMER customer)
        {
            try
            {
                // TODO: Add insert logic here
                db.TBL_CUSTOMER.Add(customer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.TBL_CUSTOMER.Where(x => x.ID == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, TBL_CUSTOMER customer)
        {
            try
            {
                // TODO: Add update logic here
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(db.TBL_CUSTOMER.Where(x => x.ID == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, TBL_CUSTOMER customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = db.TBL_CUSTOMER.Where(x => x.ID == id).FirstOrDefault();
                db.TBL_CUSTOMER.Remove(customer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
